
[Addressing scheme](addressing-scheme.md)

## BIRD Internet Routing Daemon

* [Global and Protocol Options](http://bird.network.cz/?get_doc&f=bird-3.html#ss3.2)
* [Remote Control (CLI Commands)](http://bird.network.cz/?get_doc&f=bird-4.html)
* [Protocols](http://bird.network.cz/?get_doc&f=bird-6.html#ss6.3)
